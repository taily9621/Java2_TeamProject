package item;

import java.util.Scanner;
import order.Order;

public class Item {
	static int count = 1;
	int index = 0;
	public String itemId;
	public int saleType; // 1 일반, 2 특별할인, 3 묶음할인
	String name;
	int price;
	public ItemType category;
	String mf;
	String sizeOption;
	int finalPrice ;
	
	public Item() {
	
	};

	public void read(Scanner sc) {
		index = count++;
		itemId = sc.next();
		category = ItemType.get(sc.nextInt()); // 1~7
		name = sc.next();
		price = sc.nextInt();
		mf = sc.next();
		sizeOption = sc.next();
	}
	
	public void print() {
		System.out.printf("%2d)", index);
		this.printOrder();
	}
	
	public void printmenu() {
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" - %-8s ", sizeOption);
	}

	public void printOrder(Order ord) {
		System.out.printf("%2d) ", index);
		printOrder();
	}

	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);
		finalPrice = price * ord.cc;
		System.out.printf("소계 : %5d원", finalPrice);
	}

	protected void printOrder() {
		System.out.printf(" [%s] %s %s\t(%5d)", itemId, category.getName(), name, price);
	}

	protected void printMenu() {
		System.out.printf(" [%s] %s %s\t%5d원", itemId, category.getName(), name, price);
	}

	public int getPrice() { // 상속에서 Overriding
		return finalPrice;
	}
	
	public void init(String indx, String code, String name, String sz) {
		if (indx.length() == 0)
			index = 0;
		else
			index = Integer.parseInt(indx);
		if (code.length() == 0)
			this.itemId = "";
		else
			this.itemId = code;
		if (name.length() == 0)
			this.name = "";
		else
			this.name = name;
		if (sz.length() == 0)
			this.sizeOption = "";
		else
			this.sizeOption = sz;
	}

	public ItemType getCategory() {
		return category;
	}

	public int getDiscount(boolean bMember) {
		return 0;
	}
	
	public boolean compare(String kwd) {
		return (kwd.equals(index + "") || kwd.equals(itemId) || kwd.equals(name));
	}
	
	public boolean compare(Item it) {
		if (compareIndex(it) && compareItemId(it) && compareName(it) && compareSizeOption(it))
			return true;
		return false;
	}

	boolean compareIndex(Item it) {
		if (it.index == 0 || this.index == it.index)
			return true;
		return false;
	}

	boolean compareItemId(Item it) {
		if (it.itemId.length() == 0 || this.itemId.equals(it.itemId))
			return true;
		return false;
	}

	boolean compareName(Item it) {
		if (it.name.length() == 0 || this.name.contains(it.name))
			return true;
		return false;
	}

	boolean compareSizeOption(Item it) {
		if (it.sizeOption.length() == 0 || sizeOption.contains(it.sizeOption)) {
			if (it.sizeOption.equals("l")) {
				if (sizeOption.indexOf("l") == sizeOption.indexOf("xl") + 1)
					return false;
			} // l이 검색되어야 하는데 l은 없고 xl만 있는 경우
			return true;
		}
		return false;
	}
	
	public int getIndex() {
		return index;
	}
}