package item;

import java.util.Scanner;
import order.Order;

public class ItemSpe extends Item {
	String yn;
	int dc;

	@Override
	public void read(Scanner sc) {
		super.read(sc);
		yn = sc.next();
		dc = sc.nextInt();
	}

	@Override
	public void print() {
		super.print();
		printSpe();
	}

	@Override
	public void printmenu() {
		super.printmenu();
		printSpe();
	}

	@Override
	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);

		if(!ord.isMember && yn.equals("y")) {
			finalPrice = price * ord.cc;
			System.out.printf("소계 : %5d원", finalPrice);
		}
		else {
			finalPrice = dc * ord.cc;
			System.out.printf("소계 : %5d원", finalPrice);
			System.out.printf(" %s/",yn.equals("y")?"회원":"전체");
			System.out.printf("특별할인  %5d원", dc);
			System.out.printf("<%d원 할인>  ", (price - dc));
		}
	}

	void printSpe() {
		System.out.printf(" %s/특별할인  %d원",yn.equals("y")?"회원":"전체", dc);
	}
}
