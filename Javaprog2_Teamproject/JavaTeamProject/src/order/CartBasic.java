package order;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import item.Item;
import item.ItemSet;
import item.ItemSpe;
import item.ItemType;

public class CartBasic {
	Scanner keyin = new Scanner(System.in); 
	public static ArrayList<Order> ordList = new ArrayList<>(); // order 정보 저장
	static ArrayList<Item> itemList = new ArrayList<>(); // item 정보 저장 리스트
	static ArrayList<String> members = new ArrayList<>(); // 회원 멤버 리스트
	public static ArrayList<String> buyer = new ArrayList<>(); // 구매자 리스트 (기능 추가)

	static // 멤버 초기화
	{
		members.add("ejlee");
		members.add("admin");
	};

	void doit() {
		readAllItems();
		readAllOrders();
		menu();
	}

	public static Scanner openFile(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	void readAllItems()
	// items-i-step4.txt 파일에서 item 정보를 읽어 온 후 읽어 온 정보 중 type에 따라 it 객체 생성 후 read
	// 한 뒤 itemList에 추가
	{
		Scanner fileIn = openFile("items-i-step4.txt"); // 파일에서 item 정보를 읽어 옴
		Item it = null; // Item 객체
		fileIn.nextLine(); // 한줄 공백 스킵
		int type = 1; // item 정보 타입
		while (fileIn.hasNext()) {
			type = fileIn.nextInt(); // 읽어 온 정보 중 타입 번호를 type 변수에 저장
			// 타입 별 Item 객체 생성 (1 = 일반,2 = 특별 할인,3 = 묶음 할인)
			if (type == 1) // 일반 구매
				it = new Item();
			else if (type == 2) { // 특별 할인 구매
				it = new ItemSpe(); // 특별할인
				// it.isSpe = true; // speItem으로 동작하면 Item의 띄어쓰기 부분은 생략
			} else if (type == 3) // 묶음 할인 구매
				it = new ItemSet(); // 묶음할인

			it.saleType = type; // item에 saleType을 type으로 저장
			it.read(fileIn); // item 정보를 Item의 read 실행
			it.printmenu(); // Item의 printmenu 실행
			System.out.println();
			itemList.add(it); // it 객체를 itemList 리스트에 추가
		}
		fileIn.close();
	}

	// 파일 불러오기 및 셋팅
	void readAllOrders() // orders.txt 파일에서 읽은 문장을 processAnOrder에 넘김
	{
		Scanner orderFile = openFile("orders.txt"); // 파일에서 order 정보를 읽어 옴
		while (orderFile.hasNext()) {
			processAnOrder(orderFile); // 정보를 processAnOrder 에서 실행
		}
	}

	void processAnOrder(Scanner orderFile) // 구매자 이름을 buyer 리스트에 추가 및 읽어들인 order 정보를 ord 객체에 read 한 뒤 ordList에 추가
	{
		String id = orderFile.next(); // 구매자 이름
		buyer.add(id); // 구매자 이름을 buyer 리스트에 추가
		Order ord = null; // Order 객체
		while (true) {
			ord = new Order(id); // 객체 생성
			ord.read(orderFile); // orderFile 정보로 read 를 실행
			if (ord.item == null) // read 실행 후 item 객체 정보가 없으면 반복문 중단
				break;
			ordList.add(ord); // ord 객체를 ordList에 추가
		}
	}

	void menu() {
		int kwd;
		while (true) {
			kwd = 0;
			System.out.print("(1) 상품 검색 (2) 분류별 상품 리스트 (3) 주문하기 (4) 주문 확인 (5) 상품별 주문자 검색 (6) 전체주문 (0) 종료\n>> ");
			kwd = keyin.nextInt();
			switch (kwd) {
			case 1: // 상품 검색
				searchItem();
				break;
			case 2: // 분류별 상품 리스트
				ItemList();
				break;
			case 3: // 주문하기
				sthOrder();
				break;
			case 4: // 사용자별 주문 확인
				checkOnesOrder();
				break;
			case 5: // 상품별 주문자 검색(이 상품의 주문자 : ---
				orderbySome();
				break;
			case 6: // 전체주문
				printAllOrder();
				break;
			case 0:
				return;
			}
		}
	}

	// 메뉴 1
	void searchItem() {
		String index, code, name, sz;
		Item item = new Item();
		keyin.nextLine();
		System.out.println("검색할 항목을 넣으세요. (엔터는 무시)");
		System.out.print("상품번호 : ");
		index = keyin.nextLine();
		System.out.printf("상품코드 : ");
		code = keyin.nextLine();
		System.out.printf("상품명 : ");
		name = keyin.nextLine();
		System.out.printf("사이즈 : ");
		sz = keyin.nextLine();

		item.init(index, code, name, sz);

		for (Item i : itemList) {
			if (i.compare(item)) {
				i.print();
				System.out.printf("\n");
			}
		}
	}

	// 메뉴 2
	void ItemList() {
		int num = 0;

		for (ItemType d : ItemType.values()) {
			System.out.printf("[%d] %s %n", d.getNumber(), d.getName());
		}
		
		System.out.print("검색할 분류 번호를 넣으세요... ");
		num = keyin.nextInt();
		
		for (Item it : itemList) {
			if (it.category.getNumber()==num) {
				it.print();
				System.out.println();
			}
		}
	}

	// 메뉴 3
	private void sthOrder() {// 아이디 입력 후 주문하고 주문내역 알려주기
		String userid;
		String tmp;
		Item itemid;
		int cc;
		String sz;

		Order o = null;
		while (true) {
			System.out.print("user id(end면 종료) : ");
			userid = keyin.next();
			if (userid.equals("end"))
				break;

			System.out.println("itemid 개수 사이즈(끝내려면 0) : ");
			while (true) {
				System.out.print("... ");
				tmp = keyin.next();
				if (tmp.equals("0"))
					break;
				o = new Order(userid);
				itemid = findItem(tmp);
				cc = keyin.nextInt();
				sz = keyin.next();
				o.item = itemid;
				o.cc = cc;
				o.sz = sz;
				o.userId = userid;
				ordList.add(o);
			}
			System.out.println("printOrders : " + userid);
			for (Order o1 : ordList) {
				if (o1.userId.equals(userid)) {
					o1.printOrders();
					System.out.println();
				}
			}
		}
	}

	// 메뉴 4
	void checkOnesOrder() {
		String id;
		while (true) {
			System.out.println("사용자 아이디를 넣으세요...");
			id = keyin.next();

			if (id.equals("end"))
				return;

			if (buyer.contains(id)) {
				for (Order order : ordList)
					if (order.getId().equals(id)) {
						order.print();
						System.out.println();
					}
				return;
			} else
				System.out.println("잘못된 아이디입니다. 다시 넣어 주세요...(end면 종료)");
		}
	}

	// 메뉴 5
	private void orderbySome() {
		int number;
		Item item = null;
		ArrayList<String> user = new ArrayList<>();

		System.out.println("아이템번호(1~11, 0이면 종료) : ");
		number = keyin.nextInt();
		if (number == 0)
			return;
		else if (number < 0 || number > 11) {
			System.out.println("없는 번호입니다.");
			return;
		} else {
			item = itemList.get(number - 1);

			for (Order ord : ordList)
				if (ord.item == item)
					if (!user.contains(ord.userId))
						user.add(ord.userId);

			item.print();
			System.out.println();
			if (user.size() == 0) // 주문한 사람이 한명도 없을 경우
				System.out.println("이 상품은 주문한 사람은 없습니다.");
			else
				System.out.println("이 상품의 주문자 : " + user);
		}
	}

	// 메뉴 6
	void printAllOrder() {
		ArrayList<String> users = new ArrayList<>();
		int totalPrice = 0;
		for (Order ord : ordList) {
			if (!users.contains(ord.userId))
				users.add(ord.userId);
		}

		for (String u : users) {
			totalPrice = 0;
			System.out.println("주문 확인 :" + u);
			for (Order o : ordList)
				if (o.userId.equals(u)) {
					o.print();
					totalPrice += o.Total();
					System.out.println();
				}
			System.out.println(u + "합계 : " + totalPrice + "원\n");
		}
	}

	public static Item makeSetItem(String set) {
		Item it = null;
		for (Item i : itemList)
			if (i.itemId.equals(set)) {
				it = i;
				return it;
			}
		return null;
	}

	public static Order makeSetOrder(Item it) {
		Order ord = null;
		for (Order o : ordList)
			if (o.item == it) {
				ord = o;
				return ord;
			}
		return null;
	}

	static boolean isMember(String user) {
		return members.contains(user);
	}

	public static int contains(String user, Item item) {
		for (Order ord : ordList) {
			if (ord.userId.equals(user) && ord.item == item) {
				return ord.cc;
			}
		}
		return 0;
	}

	public static boolean containSet(String user, Item item) {
		for (Order ord : ordList) {
			if (ord.userId.equals(user) && ord.item == item) {
				return true;
			}
		}
		return false;
	}

	public static Item findItem(String kwd) {
		for (Item item : itemList) {
			if (item.compare(kwd))
				return item;
		}
		return null;
	}

	public static void main(String[] args) {
		CartBasic cart = new CartBasic();
		cart.doit();
	}
}