package order;

import java.util.Scanner;
import item.ItemType;
import item.Item;

public class Order  {
	String userId;
	Item item;
	public int cc;
	public String sz;
	public boolean isMember;

	Order (String id) {
		userId = id;
		isMember = CartBasic.isMember(id);
	}
	void read(Scanner scan) {
		String tmp = scan.next();
		item = CartBasic.findItem(tmp);
		if (item == null) {
			//System.out.println("아이템 코드 못찾음" + tmp);
			return;
		}
		cc = scan.nextInt();
		sz = scan.next();
	}
	void print() {
		item.printOrderDiscount(this);
	}
	
	public void printOrders() {
		item.printOrderDiscount(this);		
	}

	 int getTotal() {
		return cc*(item.getPrice()-item.getDiscount(isMember));
	}
	// 이 사용자의 주문들 중에 it 아이템 주문 개수를 반환
	public int count(Item it) {
		return CartBasic.contains(userId, it);
	}
	// 이 주문의 아이템 개수를 반환
	public int getCount() {
		return cc;
	}
	public String getId() {
		return userId;
	}
	public Item getItem() {
		return item;
	}
	public int Total(){
		return item.getPrice();
	}
}