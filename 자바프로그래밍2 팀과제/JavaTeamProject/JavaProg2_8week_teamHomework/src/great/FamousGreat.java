package great;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
// 위인이 드라마나 영화 주제로 나온 작품이 있을 때 Great와 추가로 출력
class FamousGreat extends Great // Great 클래스를 오버라이딩
{
	ArrayList<String> dramaList = new ArrayList<>(); // 드라마 리스트
	int countDrama; // 드라마 횟수
	ArrayList<String> movieList = new ArrayList<>(); // 영화 리스트
	int countMovie; // 영화 횟수

	FamousGreat(String name) 
	{
		super(name);
	}

	public void read(Scanner in) 
	// 텍스트 파일에 영화나 드라마 글자가 있으면 영화면 어떤 영화인지 드라마면 어떤 드라마인지 리스트에 추가
	{
		super.read(in);
		countDrama = countMovie = 0;
		String line = in.nextLine().trim(); // 양 끝 공백제거
		String Lines[] = line.split(" "); // 스페이스바 단위로 문장 자르기
		for (int i = 0; i < Lines.length; i += 2) 
		{
			if (Lines[i].equals("영화")) //영화 출연 부분 추가 
			{
				movieList.add(Lines[i + 1]);
				countMovie++;
			} else if (Lines[i].equals("드라마")) // 드라마 출연 부분 추가
			{
				dramaList.add(Lines[i + 1]);
				countDrama++;
			}
		}
	}

	public void print() // 출력
	{
		super.print();
		printPlus();
	}

	void printPlus()  // 추가 출력 부분(영화, 드라마)
	{
		if (countDrama != 0 | countMovie != 0) {
			System.out.printf("\t\t\t\t\t++ ");
			if (countDrama != 0)
				printDrama();
			if (countMovie != 0)
				printMovie();
		}
		System.out.printf("\n");
	}

	void printDrama() 
	// 드라마 일때 드라마 출연작 명단 출력
	{
		System.out.printf("드라마 ");
		for (String drama : dramaList)
			System.out.printf("%s ", drama);
	}

	void printMovie() 
	// 영화 일때 영화 출연작 명단 출력
	{
		System.out.printf("영화 ");
		for (String movie : movieList)
			System.out.printf("%s ", movie);
	}

	public void printMatch(MatchType t, String kwd)
	// 검색어 중에 일치한 정보가 있으면 이 메소드로 출력
	{
		super.printMatch(t, kwd);
		if (t != MatchType.None) // 검색어가 매치되는 것이 있으면
		{
			if (t == MatchType.Drama || t == MatchType.Movie) 
			// 검색한 kwd가 드라마나 영화에 들어있을 경우
			{
				System.out.printf("\t\t\t\t\t++ ");
				if (t == MatchType.Drama) 
				// 드라마 일때 출력 부분
				{
					System.out.print("드라마 ");
					for (String d : dramaList) {
						StringBuilder sb = new StringBuilder(d);
						if (d.contains(kwd)) {
							int index = d.indexOf(kwd);
							int length = kwd.length();
							sb.insert(index, "<<");
							sb.insert(index + 2 + length, ">>");
						}
						System.out.print(sb + " ");
					}
					if (countMovie != 0)
						printMovie();
				} else if (t == MatchType.Movie) 
				// 영화 일때 출력 부분
				{
						System.out.print("영화 ");
						for (String m : movieList) {
							StringBuilder sb = new StringBuilder(m);
							if (m.contains(kwd)) {
								int index = m.indexOf(kwd);
								int length = kwd.length();
								sb.insert(index, "<<");
								sb.insert(index + 2 + length, ">>");
							}
							System.out.print(sb + " ");
						}
				}
				System.out.println();
			} else
				printPlus();
		}
	}

	public boolean compare(String kwd) 
	// 검색 키워드가 같은지 검사
	{
		boolean tmp = super.compare(kwd);
		for (String d : dramaList)
			if (d.contains(kwd)) // 검색어가 드라마 리스트에서 같은 정보가 나올 때
				return true;
		for (String m : movieList)
			if (m.contains(kwd)) // 검색어가 영화 리스트에서 같은 정보가 나올 때
				return true;
		return tmp;
	}

	public MatchType match(String kwd) 
	// 매칭 타입(드라마 or 영화)이 같은지 검사
	{
		MatchType matchType = super.match(kwd);
		for (String d : dramaList) // 드라마 일때
			if (d.contains(kwd))
				return MatchType.Drama;
		for (String m : movieList) // 영화 일때
		{
			if (m.contains(kwd))
				return MatchType.Movie;
		}
		return matchType;
	}
}
