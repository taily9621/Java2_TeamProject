package great;
import java.util.HashMap;

public enum MatchType {
	None(0, "없음"), Name(1, "이름"), Gender(2, "성별"), Age(3, "시대"), Type(4, "분류"), 
	Year(5, "년도"), Work(6, "업적"), Drama(7, "드라마"), Movie(8, "영화");
	private int num;
	private String name;
	static HashMap<Integer, MatchType> matchTypeMap = new HashMap<>();

	private MatchType(int num, String name) {
		this.num = num;
		this.name = name;
	}

	static {
		for (MatchType matchType : MatchType.values())
			matchTypeMap.put(matchType.num, matchType);
	}

	static MatchType get(int n) {
		return matchTypeMap.get(n);
	}

	static MatchType get(String str) {
		return matchTypeMap.get(str);
	}

	public int getNum() {
		return num;
	}

	public String getName() {
		return name;
	}
}
