package great;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;
//위인 찾기 프로그램
public class GreatDemo extends Manager implements Factory 
// GreatDemo 클래스는 manager 패키지의 Manager.java를 오버라이딩 하고 있으며 인터페이스인 Factory를 구현하고 있습니다.
{
	public Managable create() // manager 패키지에 인터페이스 Factory에 있는 Managable create()를 구현
	{
		return new Great();
	}
	// 
	public static void main(String[] args) // 메인 메소드 
	{
		GreatDemo demo = new GreatDemo();
		demo.doit();
	}
	
	public void doit() // 기능 실행 부분 
	{
		readAll("great-inherit.txt", this); // great-inherit.txt 파일을 읽어옴
		printAll(); // 텍스트 파일에서 읽어온 정보를 전체 출력
		search(); // 검색 메소드 실행
	}

	protected void readAll(String fileName, Factory fac) // 텍스트 파일 읽기 메소드 => 다형성
	{
		Scanner fileIn = openFile(fileName); // manager 패키지의 Manager 클래스에 있는 openFile() 메소드 실행
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			String temp = fileIn.next();
			if (temp.equals("m")) 
			// 구현된 Managable s 를 영화나 드라마에 나온 위인이면(m으로 표식) FamousGreat 클래스로 인스턴스 아닐 경우 Great()클래스로 인스턴스 함
			{
				temp = fileIn.next();
				s = new FamousGreat(temp); // 영화나 드라마에 나온 위인일때
			} else
				s = new Great(temp); // 일반 위인일때
			s.read(fileIn); // 해당 클래스의 read() 메소드 실행
			gList.add(s); // manager 패키지의 Manager 클래스에 있는 리스트 gList에 추가 
		}
		fileIn.close(); // 파일읽기 종료
	}
	
	static boolean isNumber(String str) // String 을 Int로 변환
	{
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	void search() // 검색 메소드
	{
		Scanner in = new Scanner(System.in); 
		while (true) 
		{
			System.out.printf("검색어를 입력하세요..(종료 y) : ");
			String kwd = in.nextLine();
			kwd = kwd.trim(); // 입력한 문장의 양 끝 공백 삭제
			if (kwd.equals("y")) // 종료를 할 시 반복문 탈출
				break;
			else // 검색어가 들어오면  
			{
				for (Managable m : gList)
				// 위인 정보가 저장된 gList를 비교해보고 일치하면 출력
				{
					MatchType mt = ((Great) m).match(kwd);
					// MatchType인 mt는 Great의 match 메소드 정보를 저장하고 있음
					if (m.compare(kwd)) 
					// Great의 compare() 메소드로 검색한 문장과 위인 정보가 저장된 리스트 gList에서 가져온 정보가 일치하면 Great의 printMatch() 메소드를 실행
						((Great) m).printMatch(mt, kwd); // 검색어와 일치하면 실행
				}
			}
		}
	}
}
