package great;
import java.util.HashMap;
// 나라 시대별 정보
public enum Dinasty 
{
	None(0, "없음"), GoChosun(1, "고조선"), SamKuk(2, "삼국"), Silla(3, "통일신라"), Koryo(4, "고려"), Chosun(5, "조선"), Ilje(6,
			"식민지"), KorRep(7, "대한민국");
	private int num;
	private String name;
	static HashMap<Integer, Dinasty> dinastyMap = new HashMap<>(); // 나라 시대별 정보를 HashMap에 저장

	static // 초기화 
	{
		for (Dinasty dinasty : Dinasty.values()) 
			dinastyMap.put(dinasty.num, dinasty);
	}

	private Dinasty(int num, String name) 
	// 매개변수 num, name을 Dinasty 클래스의 num, name 의 값으로 지정
	{
		this.num = num;
		this.name = name;
	}
	
	static Dinasty get(int n) // 해당 숫자에 해당하는 내용을 반환(해쉬태그) 
	{
		return dinastyMap.get(n);
	}

	static Dinasty get(String str) // 해당 단어에 해당하는 숫자를 반환
	{
		return dinastyMap.get(str);
	}

	public int getNum() // 이 클래스의 num 을 반환
	{
		return num;
	}

	public String getName() //  이 클래스의 name 을 반환
	{
		return name;
	}	
}
