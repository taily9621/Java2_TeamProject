package great;
import java.util.HashMap;
// 0 ~ 6번까지 직책 모음
public enum GreatType {
	Etc(0, "기타"), King(1, "왕"), Millitary(2, "무인"), Politician(3, "정치가"), Artist(4, "예술가"),
	Doctor(5, "의사"), Religionist(6, "종교인");
	private int num;
	private String name;
	static HashMap<Integer, GreatType> greatTypeMap = new HashMap<>();

	private GreatType(int num, String name)
	// 매배 변수 num, name 을 이 클래스의 num, name에 지정
	{
		this.num = num;
		this.name = name;
	}

	static // 초기화 
	{
		for (GreatType greatType : GreatType.values())
			greatTypeMap.put(greatType.num, greatType);
	}

	static GreatType get(int n) // 숫자에 따른 직책 명을 반환
	{
		return greatTypeMap.get(n);
	}

	static GreatType get(String str) // 직책 명에 따른 숫자를 반환
	{
		return greatTypeMap.get(str);
	}

	public int getNum() // 이 클래스의 num을 반환
	{
		return num;
	}

	public String getName() 
	{
		return name; // 이 클래스의 name을 반환
	}
}