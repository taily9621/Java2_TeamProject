package great;
import java.util.Scanner;
import manager.Managable;
// 위인 이름, 생애년도, 시기, 직책, 업적 출력
class Great implements Managable // 인터페이스 Managable 을 구현
{
	String name;
	boolean gender;
	Dinasty dinasty;
	String greatType;
	int birth;
	int death;
	String work;

	Great() // 매개 변수가 없을 경우 => empty
	{

	}

	Great(String name)  // name으로 매개변수가 있을 경우
	{
		this.name = name; // 이 클래스의 name을 매개변수 name 값으로 지정
	}

	public void read(Scanner in) // 정보를 읽어 들임 
	// read() 인터페이스 구현
	{
		System.out.printf("%s \n", name);
		gender = in.next().equals("M");
		int tmp = in.nextInt();
		dinasty = Dinasty.get(tmp);
		greatType = in.next();
		birth = in.nextInt();
		death = in.nextInt();
		work = in.nextLine();
		work = work.trim();
	}

	public void print() // 출력 메소드
	// print() 인터페이스 구현
	{
		System.out.printf("%-5s\t%s\t%4s ~ %4s %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]",
				birth > 0 ? birth : "BC" + (birth * (-1)), death > 0 ? death : "BC" + (death * (-1)), dinasty.getName(),
				greatType, work);
	}

	/*
	 * void printMovie() {
	 * 
	 * }
	 * 
	 * void printDrama() {
	 * 
	 * }
	 */
	public boolean compare(String kwd) // 비교 메소드
	//이름, 성별, 시대, 직책, 생애 년도가 일치하면 true 아니면 false
	{
		if (kwd.equals(name)) // 이름이 같을 때
			return true;
		else if (kwd.equals("M")) // 성별이 M 일때
			return true;
		else if (kwd.equals("F")) // 성별이 F 일때
			return true;
		else if (kwd.equals(dinasty.getName())) // 시대가 같을 때
			return true;
		else if (kwd.equals(greatType)) // 직책이 같을 때
			return true;
		else if (GreatDemo.isNumber(kwd)) // 생애 년도가 탄생 부터 사망 사이일 경우
		{
			int alive = Integer.parseInt(kwd);
			if (alive >= birth && alive <= death)
				return true; // 생애년도 사이일 경우
			else
				return false; // 생애년도 밖일 경우
		} else if (work.contains(kwd))
			return true;
		else
			return false;
	}

	public MatchType match(String kwd) 
	// compare 처럼 비교 후 true 이면 matchType을 해당하는 정보로 지정
	{
		MatchType matchType = MatchType.None; // matchType = (0, 없음)
		if (name.equals(kwd)) // 이름이 같으면
			matchType = MatchType.Name; // matchType = (1, 이름)
		else if (kwd.equals("M") && gender) // 남자 이면
			matchType = MatchType.Gender; // matchType = (2, 성별)
		else if (kwd.equals("F") && !gender) // 여자이면
			matchType = MatchType.Gender; // matchType = (2, 성별)
		else if (dinasty.getName().equals(kwd)) // 시대가 같으면
			matchType = MatchType.Age; // matchType = (3, 시대)
		else if (greatType.equals(kwd)) // 직책이 같으면
			matchType = MatchType.Type; // matchType = (4, 분류)
		else if (GreatDemo.isNumber(kwd)) // 입력한 년도가 생애년도 사이에 있으면
		{
			if (Integer.parseInt(kwd) >= birth && Integer.parseInt(kwd) <= death)
				matchType = MatchType.Year; // matchType = (5, 년도)
		}
		else if (work.contains(kwd)) // 키워드가 업적에 존재한다면
			matchType = MatchType.Work; // matchType = (6, 업적)
		return matchType;
	}

	public void printMatch(MatchType t, String kwd) 
	// 검색어가 위인 정보와 일치할 경우 출력
	{
		if (t == MatchType.None)
			return;

		if (t == MatchType.Name) // 이름
			System.out.printf("%-10s", "이 위인은");
		else
			System.out.printf("%s\t", name);

		if (t != MatchType.Gender && !gender) // 성별
			System.out.printf("[F]\t"); // 여자면 [F] 를 출력
		else
			System.out.printf("   \t"); // 남자면 공백 출력

		System.out.printf("%4s ~ %4s", birth > 0 ? birth : "BC" + (birth * (-1)),
				death > 0 ? death : "BC" + (death * (-1))); // 생애년도
		// 
		if (t != MatchType.Age) // 시대
			System.out.printf(" %s", dinasty.getName());
		else
			System.out.print("");

		if (t != MatchType.Type) // Great타입
			System.out.printf("\t\t%-3s\t", greatType);
		else
			System.out.print("\t\t\t");

		if (t != MatchType.Work) // 업적
			System.out.println(work);
		else {
			StringBuilder sb = new StringBuilder(work);
			int index = work.indexOf(kwd);
			int length = kwd.length();
			sb.insert(index, "<<");
			sb.insert(index + 2 + length, ">>");
			System.out.println(sb);
		}
	}
}
