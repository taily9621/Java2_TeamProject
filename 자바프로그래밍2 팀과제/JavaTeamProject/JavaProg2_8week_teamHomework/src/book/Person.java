package book;

import java.util.ArrayList;
import java.util.Scanner;

import manager.Managable;
// 
public class Person implements Managable
{
	String id;
	String name;
	String age;
	String major;
	String grade;
	String birth;
	String mf;
	String bookName;
	ArrayList<String> Booknames = new ArrayList<>();
	ArrayList<Book> myBook = new ArrayList<>();

	int tmp;

	public Person(String temp) {
		// TODO Auto-generated constructor stub
		this.id= temp;
	}

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public void read(Scanner s) {
		// TODO Auto-generated method stub
		id = s.next();
		name = s.next();
		age = s.next();
		major = s.next();
		grade = s.next();
		birth = s.next();
		mf = s.next();
		while (true) {
			bookName = s.next();
			if (bookName.equals("0"))
				break;
			Booknames.add(bookName);
		}
		Book b = null;
		for (String name : Booknames) {
			b = BookDemo.findBook(name);
			if (b != null)
				myBook.add(b);
		}
	}

	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (id.equals(kwd))
			return true;
		if (name.contains(kwd))
			return true;
		else
			return false;
	}

	public boolean loan(Book b) {
		// TODO Auto-generated method stub
		return myBook.contains(b);
	}

	public void print() {
		System.out.printf("[%s/%s] %s세, %s %s학년, %s, %s, ", id, name, age, major, grade, birth, mf);
		for (Book b : myBook)
			System.out.print(b.name + " / ");

		System.out.println(myBook.size() + "권");

	}

	public void printBook() {
		// TODO Auto-generated method stub
		System.out.printf("이름 : %s , (대출도서 : ", name);
		for (Book b : myBook)
			System.out.print(b.name + "/");
		System.out.print(myBook.size() + "권) ");
	}

}
