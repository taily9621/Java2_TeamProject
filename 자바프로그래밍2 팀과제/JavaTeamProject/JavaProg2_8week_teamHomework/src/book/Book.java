package book;

import java.util.ArrayList;
import java.util.Scanner;

import manager.Managable;

public class Book implements Managable 
{
	public int num;
	String name;
	String publisher;
	String isbn;
	String year;
	ArrayList<String> writer = new ArrayList<>();
	String sign;
	int cnt;
	int i = 0;
	String location;
	String field;
	boolean loan;
	String prewriter;
	public Book(){
		
	}
	public Book(String temp) {
		// TODO Auto-generated constructor stub
		this.name= temp;
	}

	public void read(Scanner s) {
		// TODO Auto-generated method stub
		name = s.next();
		publisher = s.next();
		isbn = s.next();
		year = s.next();
		while (true) {
			prewriter = s.next();
			if (prewriter.equals("0"))
				break;
			writer.add(prewriter);
		}
		sign = s.next();
		cnt = s.nextInt();
		location = s.next();
		field = s.next();
		if (s.hasNext())
			s.nextLine();

	}

	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (name.contains(kwd))
			return true;
		if (isbn.equals(kwd))
			return true;
		if (writer.equals(kwd))
			return true;
		else
			return false;
	}

	public void print() {
		// TODO Auto-generated method stub
		System.out.printf("[%d] %s %s (%s) %s년도 %s", num, name, publisher, isbn, year, writer);

		if (cnt > 0) {
			System.out.printf("저. <%s : %s/%s> - %d권", sign, location, field, cnt);
			System.out.print(" 대출가능");
		} else {
			System.out.printf("저. <%s : %s/%s> - ", sign, location, field);
			System.out.print(" 대출 불가");
		}
		System.out.println();

	}

	public void printexceptNumber() {
		// TODO Auto-generated method stub
		System.out.printf("%s %s (%s) %s년도 %s", name, publisher, isbn, year, writer);

		if (cnt > 0) {
			System.out.printf("저. <%s : %s/%s> - %d권", sign, location, field, cnt);
			System.out.print(" 대출가능");
		} else {
			System.out.printf("저. <%s : %s/%s> - ", sign, location, field);
			System.out.print(" 대출 불가");
		}
		System.out.println();
	}
}
