package manager;
import java.util.Scanner;
import great.MatchType;
// Managable 인터페이스 => Great 클래스에서 구현됨
public interface Managable 
{
	void read(Scanner scan);
	void print();
	boolean compare(String kwd);
}
