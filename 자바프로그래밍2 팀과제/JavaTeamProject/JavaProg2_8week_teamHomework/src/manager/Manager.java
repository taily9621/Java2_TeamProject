package manager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
	protected ArrayList<Managable> gList = new ArrayList<>();

	protected static Scanner openFile(String filename) // 파일 열기 메소드
	{
		//String path= Manager.class.getResource("").getPath();
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	protected void readAll(String fileName, Factory fac) // 텍스트 파일에서 읽어 온 정보를 
	{
		Scanner scan = openFile(fileName);
		Managable m = null;
		scan.nextLine();
		while (scan.hasNext()) 
		{
			m = fac.create(); // 새로운 Great 메소드 생성
			m.read(scan); // 파일에서 받은 정보를 Great 클래스의 read() 메소드에서 실행
			gList.add(m); // 리스트에 추가
		}
		scan.close();
	}

	protected Managable find(String kwd) // 검색어를 리스트 정보에서 찾는 메소드
	{
		for (Managable m : gList)
			if (m.compare(kwd)) // 일치하는 것이 있으면 반환
				return m;
		return null;
	}

	protected void printAll() // 리스트에 있는 모든 정보를 출력
	{
		for (Managable s : gList)
			s.print();
	}
}
