package manager;
// Factory 인터페이스 => GreatDemo 클래스에서 구현됨
public interface Factory {
	Managable create();
}