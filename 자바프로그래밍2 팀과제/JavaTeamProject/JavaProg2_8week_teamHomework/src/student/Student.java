package student;

import java.util.ArrayList;
import java.util.Scanner;

import manager.Managable;
// 인터페이스인 Managable을 이용한 Student 클래스
class Student implements Managable // manager 패키지의 Managable 인터페이스를 구현
{
	String id;
	String name;
	int year;
	boolean bMale;
	private ArrayList<Integer> scores = new ArrayList<>();
	double avg;

	public void read(Scanner s) // 파일 정보를 읽어 옴(인터페이스 구현)
	{
		id = s.next();
		name = s.next();
		year = s.nextInt();
		bMale = s.next().equals("m");
	}
	public void print() // 출력 메소드 (인터페이스 구현)
	{
		System.out.printf("%s %s %2d %s ", 
						id, name, year, bMale?"남":"녀", avg);
		if (avg > 0) System.out.printf("%5.2f%n", avg);
		else System.out.println();
	}
	public boolean compare(String kwd) // 이름 또는 아이디가 같은지 검사 (인터페이스로 구현)
	{
		return name.equals(kwd) || id.equals(kwd); // 둘 중 하나라도 성립되면 그 값을 반환
	}
	void readScores(Scanner in) 
	// 학생이 받은 점수들을 scores 리스트에 추가후 평균값을 계산
	{
		int num, sum=0;
		while (true) // 학생의 점수를 리스트에 추가
		{
			num = in.nextInt();
			if (num == 0) break;
			scores.add(num);
			sum += num;
		}
		avg = (double)sum/scores.size(); // 평균값 계산
	}
}
