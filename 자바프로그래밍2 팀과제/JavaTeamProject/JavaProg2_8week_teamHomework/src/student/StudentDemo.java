package student;

import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;
// 인터페이스를 이용한 학생 검색 프로그램
public class StudentDemo extends Manager implements Factory // manager 패키지의 Manager 클래스를 오버라이딩 하고 Factory 인터페이스를 StudentDemo에서 구현
{
	public static void main(String[] args) // 메인 메소드
	{
		StudentDemo demo = new StudentDemo();
		demo.doit();
	}

	void doit() // 실행 메소드 
	{
		readAll("students.txt",this);
		readScores();
		printAll();
	}

	void readScores() // 점수를 읽어오는 메소드
	{
		Scanner keyin = new Scanner(System.in);
		System.out.println("학생 아이디와 점수 입력 (점수 끝은 0)");
		String id;
		Managable st = null;
		while (true) {
			System.out.print("... ");
			id = keyin.next();
			st = (Student)find(id);
			if (st == null)
				break;
			((Student) st).readScores(keyin); // Student 클래스의 readScores를 실행
			;
		}
	}

	@Override
	public Student create() // Factory 인터페이스 구현
	{
		return new Student();
	}

}